var express         = require("express");
var app             = express();
var mongoose        = require("mongoose");
var bodyparser      = require("body-parser");
var cors            = require("cors");

mongoose.connect("mongodb://localhost/filter");


app.use(cors());
app.use(bodyparser.json());


var testSchema=new mongoose.Schema({
    name:String,
    level:String,
    status:String,
    type:String
});

var Test=mongoose.model("Test",testSchema);


app.post('/alltest/:page', function(req, res, next) {
   

  

    var perPage = req.body.perPage;
    
    var page = req.params.page || 1

    Test
        .find(req.body.test).sort({name:req.body.sort})
        .skip((perPage * page) - perPage)
        .limit(perPage)
        .exec(function(err, tests) {



            Test.count(req.body.test).exec(function(err, count) {
                if (err) return next(err)
                res.json({
                    tests: tests,
                    pages: Math.ceil(count / perPage)
                })
            });


                
     
        })
})


app.post("/addtest",function(req,res){
Test.create(req.body,function(err,createdtest){
    if(err){
        console.log(err);
    }else{
    
        res.json(createdtest);
    }
});
});

app.listen(4800,function(err){
    if(err){
        console.log(err);
    }else{
        console.log("server working");
    } 
});